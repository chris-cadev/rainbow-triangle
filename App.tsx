/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import {
  Canvas,
  Path,
  Rect,
  useClockValue,
  useComputedValue,
  useTiming,
  useValue,
  vec,
} from '@shopify/react-native-skia';
import React, {FC, useCallback, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

const rainbowColors = [
  'red',
  'orange',
  'yellow',
  'green',
  'cyan',
  'blue',
  'purple',
];

const windowDimensions = Dimensions.get('window');

const equilateralTriangleHeight = (side: number) => (Math.sqrt(3) * side) / 2;

const {width: screenWidth, height: screenHeight} = windowDimensions;
const squareWidth = screenWidth / rainbowColors.length;
const triangleHeight = equilateralTriangleHeight(squareWidth);
const triangleCorners = [
  [0, triangleHeight],
  [squareWidth / 2, 0],
  [squareWidth, triangleHeight],
];

const trianglePositions = Array.from(new Array(rainbowColors.length)).map(
  (_, i) => ({
    x: squareWidth * i,
    y: screenHeight - 145,
  }),
);

type Position = {x: number; y: number};
const Triangle: FC<{position: Position}> = ({position}) => {
  let trianglePath = triangleCorners
    .map(([x, y]) => [x + position.x, y + position.y].join(' '))
    .join(' L ');
  trianglePath = `M ${trianglePath} z`;
  return <Path path={trianglePath} color="black" />;
};

const getTrianglePosition = (index: number) => trianglePositions[index];

const randomNumber = (min: number, max: number) => {
  return Math.floor(min + Math.random() * (max - min));
};

const randomItem = (array: any[]) => {
  const min = 0;
  const max = array.length;
  return array[randomNumber(min, max)];
};

const App = () => {
  const minTriangleIndex = 0;
  const maxTriangleIndex = trianglePositions.length - 1;
  const [trianglePositionIndex, setTrianglePositionIndex] = useState(3);
  const moveTriangleRight = useCallback(() => {
    setTrianglePositionIndex(state => {
      const index = state + 1;
      return index > maxTriangleIndex ? maxTriangleIndex : index;
    });
  }, [maxTriangleIndex]);
  const moveTriangleLeft = useCallback(() => {
    setTrianglePositionIndex(state => {
      const index = state - 1;
      return index < minTriangleIndex ? minTriangleIndex : index;
    });
  }, [minTriangleIndex]);
  const [backgroundColor] = useState(randomItem(rainbowColors));
  const clock = useClockValue();
  const rainbowVelocity = useValue(vec(0, 200));
  const rainbowPosition = useComputedValue(() => {
    const clockSeconds = Number((clock.current / 1000).toFixed(5));
    const nextPosition =
      (clockSeconds * rainbowVelocity.current.y) % screenHeight;
    return nextPosition;
  }, [rainbowVelocity, clock]);

  rainbowPosition.addListener(value => {
    if (Math.floor(value) === 0) console.log('start');
  });

  return (
    <>
      <Canvas style={styles.canvas}>
        <Rect
          width={screenWidth}
          height={screenHeight}
          x={0}
          y={0}
          color={backgroundColor}
        />
        {rainbowColors.map((color, index) => {
          return (
            <Rect
              key={index}
              x={index * squareWidth}
              y={rainbowPosition}
              width={squareWidth}
              height={squareWidth}
              color={color}
            />
          );
        })}
        <Triangle {...{position: getTrianglePosition(trianglePositionIndex)}} />
      </Canvas>
      <View style={styles.actionsContainer}>
        <TouchableHighlight
          style={styles.button}
          onPress={() => {
            moveTriangleLeft();
          }}>
          <Text style={styles.buttonText}>{'<'}</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.button}
          onPress={() => {
            moveTriangleRight();
          }}>
          <Text style={styles.buttonText}>{'>'}</Text>
        </TouchableHighlight>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  canvas: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  actionsContainer: {
    position: 'absolute',
    bottom: 0,
    justifyContent: 'space-around',
    flexDirection: 'row',
    backgroundColor: 'black',
  },
  button: {
    flex: 1,
    alignItems: 'center',
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 50,
    color: 'white',
  },
});

export default App;
